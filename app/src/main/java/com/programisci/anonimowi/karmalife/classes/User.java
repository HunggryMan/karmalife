package com.programisci.anonimowi.karmalife.classes;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.programisci.anonimowi.karmalife.data.KarmaLifeDbHelper;

import static com.programisci.anonimowi.karmalife.data.KarmaLifeContract.UsersEntry;

/**
 * Created by michal on 25.11.17.
 */

public class User {

    private String loginId;
    private String name;

    public String getLoginId() {
        return loginId;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public Level getLevel() {
        return level;
    }

    public int getAvatarId() {
        return avatarId;
    }



    private String password;
    private int exp;
    Level level;
    private int avatarId;

    public User(int avatarId, String loginId, String password) {
        this.loginId = loginId;
        this.name = "";
        this.password = password;
        this.avatarId = avatarId;
        this.level = null;
    }

//    public User(String loginId, String name, String password, int exp, Level level, int avatarId) {
//        this.loginId = loginId;
//        this.name = name;
//        this.password = password;
//        this.exp = exp;
//        this.level = level;
//        this.avatarId = avatarId;
//    }

    public static User user = new User(1 ,"Maciek", "admin");

    public int getExp() {
        return exp;
    }

    public void addPoints(int exp) {
        this.exp += exp;
    }

    private int getNextLevel(Context context) {

        return 0;
    }

    public void addUserToDb(Context context){

        SQLiteDatabase db = (new KarmaLifeDbHelper(context)).getWritableDatabase();
        ContentValues valueToInsert = new ContentValues();
        valueToInsert.put(UsersEntry.COLUMN_AVATAR_ID,getAvatarId());
        valueToInsert.put(UsersEntry.COLUMN_LOGIN_ID,getLoginId());
        valueToInsert.put(UsersEntry.COLUMN_NAME,UsersEntry.NAME_DEFAULT);
        valueToInsert.put(UsersEntry.COLUMN_PASSWORD,getPassword());

        long newRowId = db.insert(UsersEntry.TABLE_NAME,null,valueToInsert);


    }



//    public int getImageFromUser(Context context){
//
////        String[] projection = { UsersEntry.COLUMN_AVATAR_ID };
////        int avatar_id = 0;
////
////         KarmaLifeDbHelper dbHelper;
////        dbHelper = new KarmaLifeDbHelper(context);
////        SQLiteDatabase db = dbHelper.getReadableDatabase();
////
////        Cursor cursor = db.query(
////                UsersEntry.TABLE_NAME,
////                projection,
////                null,
////                null,
////                null,
////                null,
////                null);
////
////        try {
////            //if(cursor != null)
////            //cursor.moveToNext();
////            //avatar_id = cursor.getInt(cursor.getColumnIndex(UsersEntry.COLUMN_AVATAR_ID));
////            avatar_id = R.drawable.avatar_girl2;
////        } finally {
////            if(cursor != null) cursor.close();
////        }
////    }
//
//       return avatarId;

}
