package com.programisci.anonimowi.karmalife.classes;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.programisci.anonimowi.karmalife.data.KarmaLifeDbHelper;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.programisci.anonimowi.karmalife.data.KarmaLifeContract.*;

/**
 * A class that describes a diary entry.
 *
 * Created by RaV on 25.11.2017.
 */

public class Diary {

    private String diaryEntry;
    private  int year;
    private int month;
    private int day;
    private int mood;


    public Diary(String diaryEntry,int mood,Context context){

        this.diaryEntry = diaryEntry;

        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());

        StringBuilder yearBuilder = new StringBuilder(currentDateTimeString.substring(0,4));
        this.year=Integer.parseInt(yearBuilder.toString());

        StringBuilder monthBuilder = new StringBuilder(currentDateTimeString.substring(5,7));
        this.month=Integer.parseInt(monthBuilder.toString());

        StringBuilder dayBuilder = new StringBuilder(currentDateTimeString.substring(8,10));
        this.day=Integer.parseInt(dayBuilder.toString());

        this.mood = mood;


    }

    public Diary(String diaryEntry, int mood, int year, int month, int day) {
        this.diaryEntry = diaryEntry;
        this.mood = mood;
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public String getDiaryEntry(){
        return diaryEntry;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public int getMood() {
        return mood;
    }

    public static ArrayList<Diary> makeExampleDiary(){

        ArrayList<Diary> examplesDiary = new ArrayList();

        examplesDiary.add(new Diary("Miałem dzisiaj wspanialy dzien, swiecilo piekne slonce",4,2017,11,12));
        examplesDiary.add(new Diary("Miałem dzisiaj okrpny dzien, nie chce o tym pisac",1,2017,11,15));
        examplesDiary.add(new Diary("Najgorszy dzien w moim zyciu, umarl mi kot i chomik",0,2017,11,20));
        examplesDiary.add(new Diary("Bylem dzisiaj na hakatonie, moglo mi pojsc lepiej ",2,2017,11,22));


        return examplesDiary;
    }

    public void insertDiaryToDataBase(Context context){

        SQLiteDatabase db = (new KarmaLifeDbHelper(context)).getWritableDatabase();
        ContentValues valueToInsert = new ContentValues();
        valueToInsert.put(DiaryEntry.COLUMN_DATE_DAY, getDay());
        valueToInsert.put(DiaryEntry.COLUMN_DATE_YEAR, getYear());
        valueToInsert.put(DiaryEntry.COLUMN_DATE_MONTH, getMonth());
        valueToInsert.put(DiaryEntry.COLUMN_DIARY_ENTRY, getDiaryEntry());
        valueToInsert.put(DiaryEntry.COLUMN_MOOD, getMood());

        long newRowId = db.insert(DiaryEntry.TABLE_NAME,null,valueToInsert);

        if(newRowId == -1){
            Toast.makeText(context, "Chujowo :/", Toast.LENGTH_SHORT).show();
        }





    }

    public static List<Diary> getEntriesFromMonth(int month, Context context) {
        SQLiteDatabase db = (new KarmaLifeDbHelper(context)).getReadableDatabase();

        String[] selectionArgs = { Integer.toString(month) };

        String[] columns = {
                DiaryEntry.COLUMN_DIARY_ENTRY,
                DiaryEntry.COLUMN_MOOD,
                DiaryEntry.COLUMN_DATE_DAY,
                DiaryEntry.COLUMN_DATE_MONTH,
                DiaryEntry.COLUMN_DATE_YEAR };

        Cursor cursor = db.query(
                DiaryEntry.TABLE_NAME,
                columns,
                DiaryEntry.COLUMN_DATE_MONTH,
                selectionArgs,
                null,
                null,
                null);

        List<Diary> list = new ArrayList<Diary>();

        while(cursor.moveToNext()) {
            list.add(new Diary(
                    cursor.getString(cursor.getColumnIndex(DiaryEntry.COLUMN_DIARY_ENTRY)),
                    cursor.getInt(cursor.getColumnIndex(DiaryEntry.COLUMN_MOOD)),
                    cursor.getInt(cursor.getColumnIndex(DiaryEntry.COLUMN_DATE_YEAR)),
                    cursor.getInt(cursor.getColumnIndex(DiaryEntry.COLUMN_DATE_MONTH)),
                    cursor.getInt(cursor.getColumnIndex(DiaryEntry.COLUMN_DATE_DAY))));
        }

        return list;
    }

    }

