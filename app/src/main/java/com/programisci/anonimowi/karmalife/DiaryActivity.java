package com.programisci.anonimowi.karmalife;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.programisci.anonimowi.karmalife.classes.Diary;
import com.programisci.anonimowi.karmalife.classes.DiaryAdapter;

import java.util.ArrayList;

public class DiaryActivity extends AppCompatActivity {

    private ListView mlistView ;
    private DiaryAdapter mDiaryAdapter;
    final Context context = this;
    private Button tickButtonDialog;
    private ImageButton avatarButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diary);
        mlistView = findViewById(R.id.listNotes);
        initialize();
    }

    private void initialize(){


        ArrayList<Diary> notesToDisplay = Diary.makeExampleDiary();

        mDiaryAdapter = new DiaryAdapter(this, notesToDisplay);



        mlistView.setAdapter(mDiaryAdapter);
        mlistView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view,
                                           int position, long id) {
                showDialogMessage();


                return false;
            }
        });
    }

    private void showDialogMessage(){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.mission_dialog);

        tickButtonDialog = dialog.findViewById(R.id.tick_button_mission_dialog);
        tickButtonDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
