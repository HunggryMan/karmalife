package com.programisci.anonimowi.karmalife;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;

import com.programisci.anonimowi.karmalife.classes.Diary;
import com.programisci.anonimowi.karmalife.classes.DiaryAdapter;
import com.programisci.anonimowi.karmalife.classes.Level;
import com.programisci.anonimowi.karmalife.classes.Mission;
import com.programisci.anonimowi.karmalife.data.KarmaLifeContract.*;

import java.util.ArrayList;
import java.util.Random;

import static android.os.Build.VERSION_CODES.M;

public class MainActivity extends AppCompatActivity {

    private  ListView mlistView;

    private ImageButton mMissionButtons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ArrayList currentMission = Mission.getCurrentMissionsArrayList(this,-1);
        setContentView(R.layout.activity_main);
        mlistView = findViewById(R.id.listNotes_main);

        //TODO TODO todotodotodo https://www.youtube.com/watch?v=9OPc7MRm4Y8
        initialize();
    }

    private void initialize(){

        ArrayList<Diary> notesToDisplay = Diary.makeExampleDiary();

        DiaryAdapter mDiaryAdapter = new DiaryAdapter(this, notesToDisplay);



        mlistView.setAdapter(mDiaryAdapter);
        mMissionButtons = (ImageButton) findViewById(R.id.mission_button);
        mMissionButtons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, MissionsActivity.class));
            }
        });

    }





}