package com.programisci.anonimowi.karmalife;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.programisci.anonimowi.karmalife.classes.User;

public class RegistrationActivity extends AppCompatActivity {

    private ImageButton mAvatarMan1;
    private ImageButton mAvatarMan2;
    private ImageButton mAvatarGirl1;
    private ImageButton mAvatarGirl2;
    private EditText loginEditText;
    private EditText passwordEditText;
    private Button submitButton;
    private int avatarId = 0;
    private String login;
    private String pass;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        initialize();
    }

    private void initialize() {
        mAvatarMan1 = findViewById(R.id.avatar_m_1);
        mAvatarMan1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeTransparent();
                mAvatarMan1.setAlpha(255);
                avatarId = 0;
            }
        });

        mAvatarMan2 = findViewById(R.id.avatar_m_2);
        mAvatarMan2.setAlpha(50);
        mAvatarMan2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeTransparent();
                mAvatarMan2.setAlpha(255);
                avatarId = 1;
            }
        });

        mAvatarGirl1 = findViewById(R.id.avatar_girl_1);
        mAvatarGirl1.setAlpha(50);
        mAvatarGirl1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeTransparent();
                mAvatarGirl1.setAlpha(255);
                avatarId = 2;
            }
        });

        mAvatarGirl2 = findViewById(R.id.avatar_girl_2);
        mAvatarGirl2.setAlpha(50);
        mAvatarGirl2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeTransparent();
                mAvatarGirl2.setAlpha(255);
                avatarId = 3;
            }
        });
        loginEditText = findViewById(R.id.login);
        passwordEditText = findViewById(R.id.password);
        submitButton = findViewById(R.id.submit_button_reg);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendData();
            }
        });

    }

    private void makeTransparent(){
        mAvatarMan1.setAlpha(50);
        mAvatarMan2.setAlpha(50);
        mAvatarGirl1.setAlpha(50);
        mAvatarGirl2.setAlpha(50);

    }

    private void sendData(){
        login = loginEditText.getText().toString();
        pass = passwordEditText.getText().toString();

        User user = new User(avatarId, login, pass);
        user.addUserToDb(this);
        startActivity(new Intent(RegistrationActivity.this, MainActivity.class));

    }

}
