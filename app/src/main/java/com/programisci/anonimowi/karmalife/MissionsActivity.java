package com.programisci.anonimowi.karmalife;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;


import com.programisci.anonimowi.karmalife.classes.Mission;
import com.programisci.anonimowi.karmalife.classes.MissionAdapter;

import java.util.ArrayList;

public class MissionsActivity extends AppCompatActivity {

    private ListView mlistView ;
    private MissionAdapter adapterMission ;
    final Context context = this;
    private Button tickButtonDialog;
    private ImageButton avatarButton;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_missions);
        mlistView = findViewById(R.id.listMissions);
        initialize();
    }

    private void initialize(){


        ArrayList<Mission> missionToDisplay = new ArrayList<Mission>();
        missionToDisplay.add(new Mission("DO SOMETHING FOR AT LEAST 1 HOUR", 2, 50));
        missionToDisplay.add(new Mission("TAKE YOUR MOM OUT", 1, 60));
        adapterMission = new MissionAdapter(this, missionToDisplay);

        mlistView.setAdapter(adapterMission);
        mlistView.setOnItemLongClickListener(new OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view,
                                           int position, long id) {
                showDialogMessage();


                return false;
            }
        });
    }

    private void showDialogMessage(){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.mission_dialog);

        tickButtonDialog = dialog.findViewById(R.id.tick_button_mission_dialog);
        tickButtonDialog.setOnClickListener(new View.OnClickListener() {
            @Override
           public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }




}
