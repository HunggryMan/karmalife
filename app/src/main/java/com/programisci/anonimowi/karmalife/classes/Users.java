package com.programisci.anonimowi.karmalife.classes;

/**
 * Created by Wojtek on 25.11.2017.
 */

public class Users {

    private String loginId;
    private String name;
    private String password;

    public Users(String loginId, String name, String password) {
        this.loginId = loginId;
        this.name = name;
        this.password = password;
    }


    public String getLoginId() {
        return loginId;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    static Users user10 = new Users("admin","Wojtek","Nap");
}