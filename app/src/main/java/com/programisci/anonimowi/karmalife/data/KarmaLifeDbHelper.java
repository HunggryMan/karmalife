package com.programisci.anonimowi.karmalife.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.programisci.anonimowi.karmalife.data.KarmaLifeContract.CurrentMissionEntry;
import com.programisci.anonimowi.karmalife.data.KarmaLifeContract.DiaryEntry;
import com.programisci.anonimowi.karmalife.data.KarmaLifeContract.LevelEntry;
import com.programisci.anonimowi.karmalife.data.KarmaLifeContract.MissionsEntry;
import com.programisci.anonimowi.karmalife.data.KarmaLifeContract.UsersEntry;


/**
 * A class that helps to get connection with database
 *
 * Created by RaV
 */

public class KarmaLifeDbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "KarmaLife.db";
    private static final int DATABASE_VERSION  = 1;


    public KarmaLifeDbHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        createMissionsTable(db);
        createDairyTable(db);
        createUsersTable(db);
        createLevelTable(db);
        createCurrentMissionsTable(db);
    }

    private void createMissionsTable(SQLiteDatabase db) {
        db.execSQL(MissionsEntry.CREATE_MISSIONS_TABLE);
    }

    private void createDairyTable(SQLiteDatabase db) {
        db.execSQL(DiaryEntry.CREATE_DIARY_TABLE);
    }

    private void createUsersTable(SQLiteDatabase db) {
        db.execSQL(UsersEntry.CREATE_USERS_TABLE);
    }

    private void createLevelTable (SQLiteDatabase db ){
        db.execSQL(LevelEntry.CREATE_LEVEL_TABLE);
    }

    private void createCurrentMissionsTable(SQLiteDatabase db) {
        db.execSQL(CurrentMissionEntry.CREATE_CURRENT_MISSIONS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    
}
