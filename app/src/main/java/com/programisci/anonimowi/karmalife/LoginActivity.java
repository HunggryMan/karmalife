package com.programisci.anonimowi.karmalife;


import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.programisci.anonimowi.karmalife.classes.User;
import com.programisci.anonimowi.karmalife.data.KarmaLifeDbHelper;

public class LoginActivity extends AppCompatActivity {

    private ImageView mAvatarImage;
    private EditText mPassword;
    private ImageButton mSubmit;

    private KarmaLifeDbHelper dbHelper;

    public static User user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if(user==null)
            startActivity(new Intent(LoginActivity.this, RegistrationActivity.class));



        mPassword = findViewById(R.id.password_edit_text);
        mAvatarImage = findViewById(R.id.avatar_image);
        mPassword.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);

        initialize();
    }











    private void initialize(){
        mPassword = findViewById(R.id.password_edit_text);

        mAvatarImage = findViewById(R.id.avatar_image);

        if(user == null)
            mAvatarImage.setImageResource(R.drawable.avatar_man1);
        else
            mAvatarImage.setImageResource(user.getAvatarId());
        mSubmit = findViewById(R.id.submit_button);
        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkPassword())
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
            }
        });
        mPassword.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
    }

    /**
     * @return Whether the password is valid or not in compare to saved password.
     */
    private boolean checkPassword() {
        String passwordToCheck = mPassword.getText().toString().trim();

        //return (passwordToCheck.length() >= UsersEntry.PASSWORD_LENGTH_MIN && passwordToCheck.length() <= UsersEntry.PASSWORD_LENGTH_MAX);
        return passwordToCheck.equals(user.getPassword());
    }
}