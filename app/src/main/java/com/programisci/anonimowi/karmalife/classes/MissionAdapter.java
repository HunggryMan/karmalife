package com.programisci.anonimowi.karmalife.classes;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.programisci.anonimowi.karmalife.R;

import java.util.ArrayList;

import static com.programisci.anonimowi.karmalife.data.KarmaLifeContract.MissionsEntry;

/**
 * Created by Wojtek on 25.11.2017.
 */

public class MissionAdapter  extends ArrayAdapter<Mission> {

    public MissionAdapter(Activity context, ArrayList<Mission> currentMissions){
        super(context, 0, currentMissions);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        Mission presentMission = getItem(position);

        // check if the existing view is being reused, otherwise inflate the view
        View missionView = convertView;
        if(missionView == null) {
            missionView = LayoutInflater.from(getContext()).inflate(
                    R.layout.mission, parent, false);
        }

        TextView mMissionDesc = missionView.findViewById(R.id.mission_desc);
        mMissionDesc.setText(presentMission.getName());

        TextView mMissionCategory = missionView.findViewById(R.id.mission_category);

        switch(presentMission.getType()) {
            case MissionsEntry.TYPE_OTHER:
                mMissionCategory.setText("OTHER");
                break;
            case MissionsEntry.TYPE_FAMILY:
                mMissionCategory.setText("FAMILY");
                break;
            case MissionsEntry.TYPE_FRIENDS:
                mMissionCategory.setText("FRIENDS");
                break;
            case MissionsEntry.TYPE_HEALTH:
                mMissionCategory.setText("HEALTH");
                break;
            case MissionsEntry.TYPE_USERS:
                mMissionCategory.setText("OWN");
                break;
            case MissionsEntry.TYPE_WORK_STUDIES:
                mMissionCategory.setText("EDUCATION");

        }


        TextView mMissionPoints = missionView.findViewById(R.id.mission_pts);
        mMissionPoints.setText("+"+presentMission.getPoints()+"PTS");


        return missionView;
    }


}
