package com.programisci.anonimowi.karmalife.classes;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.programisci.anonimowi.karmalife.R;

import java.util.ArrayList;

/**
 * Created by Wojtek on 25.11.2017.
 */

public class DiaryAdapter extends ArrayAdapter<Diary> {

    public DiaryAdapter(Activity context, ArrayList<Diary> currentNote) {
        super(context, 0, currentNote);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        Diary presentDiary = getItem(position);

        // check if the existing view is being reused, otherwise inflate the view
        View diaryView = convertView;
        if (diaryView == null) {
            diaryView = LayoutInflater.from(getContext()).inflate(
                    R.layout.note, parent, false);
        }

        TextView mDesc =diaryView.findViewById(R.id.note_desc);
        mDesc.setText(presentDiary.getDiaryEntry());

        ImageView mEmotikon = diaryView.findViewById(R.id.emotikon);


        switch (presentDiary.getMood()){

            case 0: mEmotikon.setImageResource(R.drawable.mood_awful);
            break;

            case 1: mEmotikon.setImageResource(R.drawable.mood_bad);
                break;

            case 2: mEmotikon.setImageResource(R.drawable.mood_neutral);
                break;

            case 3: mEmotikon.setImageResource(R.drawable.mood_good);
                break;

            case 4: mEmotikon.setImageResource(R.drawable.mood_excellent);
                break;
        }

        TextView mDate = diaryView.findViewById(R.id.date_note);
        mDate.setText(presentDiary.getDay() + "." + presentDiary.getMonth() + "." + presentDiary.getYear());




        TextView mMissionCategory = diaryView.findViewById(R.id.mission_category);

        return diaryView;
    }
}
