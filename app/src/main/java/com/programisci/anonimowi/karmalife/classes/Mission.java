package com.programisci.anonimowi.karmalife.classes;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.programisci.anonimowi.karmalife.data.KarmaLifeDbHelper;

import java.util.ArrayList;
import java.util.Random;

import static com.programisci.anonimowi.karmalife.data.KarmaLifeContract.CurrentMissionEntry;
import static com.programisci.anonimowi.karmalife.data.KarmaLifeContract.MissionsEntry;

/**
 * A class that describes mission stored in database.
 *
 * Created by RaV on 25.11.2017.
 */
public class Mission {

    /**
     * Short description.
     */
    private String name;
    /**
     * Category of the mission.
     */
    private int type;
    /**
     * Points each mission give player
     */
    private int points;



    private static int USER_TASK = 5;

    public Mission(String name, int type, int points) {
        this.name = name;
        this.type = type;
        this.points = points;
    }

    public Mission(String name, int type) {
        this.name = name;
        this.type = type;
        this.points = 10;
    }

    public String getName() {
        return name;
    }

    public int getType() {
        return type;
    }

    public int getPoints() {
        return points;
    }


    private static boolean isTableEmpty(Context context) {
        SQLiteDatabase db = (new KarmaLifeDbHelper(context)).getReadableDatabase();

        Cursor cursor = db.query(
                MissionsEntry.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null
        );

        return cursor.getCount() == 0;
    }

    public static ArrayList<Mission> systemMissions() {


        ArrayList<Mission> missions = new ArrayList<>();

        missions.add(new Mission("Make your friend's day with nice words", MissionsEntry.TYPE_FRIENDS, 10));
        missions.add(new Mission("Tell parents you love them", MissionsEntry.TYPE_FAMILY, 10));
        missions.add(new Mission("Help someone with his/her homework", MissionsEntry.TYPE_WORK_STUDIES, 10));
        missions.add(new Mission("Give random child a candy", MissionsEntry.TYPE_OTHER, 10));
        missions.add(new Mission("Give your friend a flower", MissionsEntry.TYPE_FRIENDS, 10));
        missions.add(new Mission("Exercise in the morning AND  in the evening", MissionsEntry.TYPE_HEALTH, 10));
        missions.add(new Mission("Go to bed early", MissionsEntry.TYPE_HEALTH, 10));
        missions.add(new Mission("No swear day", MissionsEntry.TYPE_OTHER, 10));
        missions.add(new Mission("Eat a dinner with your family", MissionsEntry.TYPE_FAMILY, 10));
        missions.add(new Mission("Get along with your mates/collegues ", MissionsEntry.TYPE_WORK_STUDIES, 10));
        missions.add(new Mission("Be on every meeting/lecture in current week", MissionsEntry.TYPE_WORK_STUDIES, 10));
        missions.add(new Mission("Don't be late in this week", MissionsEntry.TYPE_WORK_STUDIES, 10));
        missions.add(new Mission("No alcohol week", MissionsEntry.TYPE_HEALTH, 10));
        missions.add(new Mission("Attend a gig of a locale band", MissionsEntry.TYPE_FRIENDS, 10));
        missions.add(new Mission("Organize a dinner for your whole family", MissionsEntry.TYPE_FAMILY, 10));
        missions.add(new Mission("Get along with your siblings", MissionsEntry.TYPE_FAMILY, 10));
        missions.add(new Mission("Attend a charity event in your local area", MissionsEntry.TYPE_OTHER, 10));
        missions.add(new Mission("Learn something new", MissionsEntry.TYPE_WORK_STUDIES, 10));


        return missions;

        }


    public static final int NO_ACTION = -1;
    public static final int DELETE_FROM_POS_1 = 0;
    public static final int DELETE_FROM_POS_2 = 1;
    public static final int DELETE_FROM_POS_3 = 2;

    /**
     *
     * @param context
     * @param pos -1 -> don't delete, 0/1/2 - delete from this position
     * @return
     */

    public static ArrayList<Mission> getCurrentMissionsArrayList(Context context, int pos) {

        String[] projection = {
                MissionsEntry._ID,
                MissionsEntry.COLUMN_NAME,
                MissionsEntry.COLUMN_POINTS,
                MissionsEntry.COLUMN_TYPE};

        KarmaLifeDbHelper dbHelper = new KarmaLifeDbHelper(context);

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        Cursor cursor = db.query(
                MissionsEntry.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null);

        if (cursor == null)
            throw new IllegalArgumentException("Baza nie zwróciła żadnych wyników!");

        int cursorSize = cursor.getCount();
        Random generator = new Random();
        ArrayList<Mission> currentMissions = new ArrayList<>();

        switch(pos) {
            case NO_ACTION:

                break;
            case DELETE_FROM_POS_1:

                break;
            case DELETE_FROM_POS_2:

                break;
            case DELETE_FROM_POS_3:

                break;
            default:
                Toast.makeText(context, "Zepsułeś, Wojciechu, podałeś złą pozycję :<", Toast.LENGTH_SHORT).show();
        }

        try {
            while (currentMissions.size() < 3) {

                int missionId = generator.nextInt(cursorSize);

                //TODO change way to move cursor to generated position MICHAL
                for (int i = missionId; i > 0; --i)
                    cursor.moveToNext();

                String name = cursor.getString(cursor.getColumnIndex(MissionsEntry.COLUMN_NAME));
                int type = cursor.getInt(cursor.getColumnIndex(MissionsEntry.COLUMN_TYPE));
                int points = cursor.getInt(cursor.getColumnIndex(MissionsEntry.COLUMN_POINTS));

                Mission current = new Mission(name, type, points);

                ContentValues values = new ContentValues();
                values.put(CurrentMissionEntry.COLUMN_MISSION_ID, cursor.getInt(cursor.getColumnIndex(MissionsEntry._ID)));

                SQLiteDatabase database = dbHelper.getWritableDatabase();

                long newRowId = database.insert(CurrentMissionEntry.TABLE_NAME, null, values);

                if(newRowId == -1)
                    Toast.makeText(context, "Loading current mission failed!", Toast.LENGTH_SHORT).show();
                else
                    currentMissions.add(current);

                cursor.moveToFirst();
            }
            //if(cursor != null)
                //cursor.close();
            return currentMissions;

        } finally {
            if(cursor != null || !cursor.isClosed())
                cursor.close();
        }
    }

    public static void addMission(String textOfTask,Context context){

        Mission userMission = new Mission(textOfTask,USER_TASK);
        SQLiteDatabase db = (new KarmaLifeDbHelper(context)).getWritableDatabase();
        ContentValues valueToInsert = new ContentValues();

        valueToInsert.put(MissionsEntry.COLUMN_NAME,userMission.getName());
        valueToInsert.put(MissionsEntry.COLUMN_TYPE,userMission.getType());
        valueToInsert.put(MissionsEntry.COLUMN_POINTS,userMission.getPoints());

        long newRowId = db.insert(MissionsEntry.TABLE_NAME, null, valueToInsert);

        newRowId = db.insert(MissionsEntry.TABLE_NAME,null,valueToInsert);

        if (newRowId == -1)
            Toast.makeText(context, "Ups, nie dodało się :/", Toast.LENGTH_SHORT).show();

    }

}
