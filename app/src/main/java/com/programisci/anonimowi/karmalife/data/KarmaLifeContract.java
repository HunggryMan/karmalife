package com.programisci.anonimowi.karmalife.data;

import android.provider.BaseColumns;

/**
 * A class that represents constants and sanity checks connected with data base
 *
 * Created by RaV
 */
public final class KarmaLifeContract {

    private KarmaLifeContract() {}

//    public static final String CONTENT_AUTHORITY = "com.programisci.anonimowi.karmalife";
//    public static final Uri BASE_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
//
//    public static final String PATH_MISSIONS = "missions";
//    public static final String PATH_DIARY = "diary";
//    public static final String PATH_USERS = "users";
//    public static final String PATH_LEVELS = "levels";

    public static final class MissionsEntry {

            public static final String TABLE_NAME = "missions";

            public static final String _ID = BaseColumns._ID;
            public static final String COLUMN_NAME = "name";
            public static final String COLUMN_TYPE = "type";
            public static final String COLUMN_POINTS = "points";

            public static final int TYPE_OTHER = 0;
            public static final int TYPE_FAMILY = 1;
            public static final int TYPE_HEALTH = 2;
            public static final int TYPE_FRIENDS = 3;
            public static final int TYPE_WORK_STUDIES = 4;
            public static final int TYPE_USERS = 5;

            public static final String CREATE_MISSIONS_TABLE = "CREATE TABLE " + MissionsEntry.TABLE_NAME + "("
                    + MissionsEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + MissionsEntry.COLUMN_NAME + " NOT NULL, "
                    + MissionsEntry.COLUMN_TYPE + " INTEGER NOT NULL DEFAULT " + MissionsEntry.TYPE_OTHER + ", "
                    + MissionsEntry.COLUMN_POINTS + " INTEGER NOT NULL DEFAULT " + MissionsEntry.POINTS_DEFAULT + ");";

            //public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_URI, PATH_MISSIONS);

            public static final int POINTS_MIN = 1;
            public static final int POINTS_MAX = 20;

            public static final int POINTS_DEFAULT = 10;

            /**
             * Checks whether inserted type is valid.
             *
             * @param t Type
             * @return Is valid(true) or not (false).
             */
            public static boolean isValidType(int t) {
                return (t == TYPE_OTHER
                        || t == TYPE_FAMILY
                        || t == TYPE_HEALTH
                        || t == TYPE_FRIENDS
                        || t == TYPE_WORK_STUDIES
                        || t == TYPE_USERS);
            }

            /**
             * Checks whether inserted points value is valid.
             *
             * @param p Points
             * @return Is valid(true) or not (false).
             */
            public static boolean isValidPoints(int p) {
                return (p >= POINTS_MIN && p <= POINTS_MAX);
            }
        }

    public static final class DiaryEntry {
        public static final String TABLE_NAME = "diary";

        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_DIARY_ENTRY = "diary_entry";
        public static final String COLUMN_DATE_YEAR = "date_of_entry_year";
        public static final String COLUMN_DATE_MONTH = "date_of_entry_month";
        public static final String COLUMN_DATE_DAY = "date_of_entry_day";
        public static final String COLUMN_MOOD = "mood";

        public static final int MOOD_AWFUL = 0;
        public static final int MOOD_BAD = 1;
        public static final int MOOD_NEUTRAL = 2;
        public static final int MOOD_GOOD = 3;
        public static final int MOOD_EXCELLENT = 4;

        public static final String CREATE_DIARY_TABLE = "CREATE TABLE " + DiaryEntry.TABLE_NAME + "("
                + DiaryEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DiaryEntry.COLUMN_DIARY_ENTRY + " TEXT NOT NULL, "
                + DiaryEntry.COLUMN_DATE_YEAR + " INTEGER NOT NULL, "
                + DiaryEntry.COLUMN_DATE_MONTH + " INTEGER NOT NULL, "
                + DiaryEntry.COLUMN_DATE_DAY + " INTEGER NOT NULL, "
                + DiaryEntry.COLUMN_MOOD + " INTEGER NOT NULL DEFAULT " + DiaryEntry.MOOD_NEUTRAL + ");";

        //public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_URI, PATH_DIARY);

        /**
         * Checks whether inserted mood is valid.
         * @param m Mood
         * @return Is valid(true) or not (false).
         */
        public static boolean isMoodValid(int m) {
            return (m == MOOD_AWFUL
                    || m == MOOD_BAD
                    || m == MOOD_NEUTRAL
                    || m == MOOD_GOOD
                    || m == MOOD_EXCELLENT);
        }
    }

    public static final class UsersEntry{

        public static final String TABLE_NAME="users";

        public static final String COLUMN_LOGIN_ID = "login";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_PASSWORD = "password";
        public static final String COLUMN_LEVEL = "level";
        public static final String COLUMN_EXP = "exp";
        public static final String COLUMN_AVATAR_ID = "avatar";

        public static final int EXP_DEFAULT = 0;

        public static final int PASSWORD_LENGTH_MAX = 13;
        public static final int PASSWORD_LENGTH_MIN = 6;

        public static final String NAME_DEFAULT = "";

        public static final String CREATE_USERS_TABLE = "CREATE TABLE " + UsersEntry.TABLE_NAME  + "("
                + UsersEntry.COLUMN_LOGIN_ID + " TEXT PRIMARY KEY, "
                + UsersEntry.COLUMN_NAME + " TEXT, "
                + UsersEntry.COLUMN_PASSWORD + " TEXT NOT NULL, "
                + UsersEntry.COLUMN_EXP + " INTEGER NOT NULL DEFAULT " + UsersEntry.EXP_DEFAULT + ", "
                + UsersEntry.COLUMN_AVATAR_ID + " INTEGER NOT NULL, "
                + UsersEntry.COLUMN_LEVEL + " INTEGER NOT NULL DEFAULT 0, "
                + "FOREIGN KEY(" + UsersEntry.COLUMN_LEVEL
                + ") REFERENCES " + LevelEntry.TABLE_NAME + "(" + LevelEntry._ID + "));";


        //public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_URI, PATH_USERS);
    }

    public static final class LevelEntry{

        public static final String TABLE_NAME = "levels";

        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_LEVEL_VALUE = "level_value";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_EXP_MIN = "exp_min";
        public static final String COLUMN_EXP_MAX = "exp_max";

        //public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_URI, PATH_LEVELS);

        public static final int LEVEL_VALUE_DEFAULT = 0;
        public static final int LEVEL_VALUE_MAX = 9;
        public static final int LEVEL_VALUE_MIN = -6;

        public static final String CREATE_LEVEL_TABLE = " CREATE TABLE "+LevelEntry.TABLE_NAME + " ( "
                +LevelEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                +LevelEntry.COLUMN_LEVEL_VALUE + " INTEGER DEFAULT " + LevelEntry.LEVEL_VALUE_DEFAULT + ", "
                +LevelEntry.COLUMN_NAME + " TEXT NOT NULL, "
                +LevelEntry.COLUMN_EXP_MAX + " INTEGER NOT NULL, "
                +LevelEntry.COLUMN_EXP_MIN + " INTEGER NOT NULL);";

        /**
         * Checks whether inserted level value is valid.
         * @param lv Level value.
         * @return Is valid(true) or not (false).
         */
        public static boolean isLevelValueValid(int lv) {
            return (lv >= LEVEL_VALUE_MIN && lv <= LEVEL_VALUE_MAX);
        }
    }

    public static final class CurrentMissionEntry {

        public static final String TABLE_NAME = "current_mission";

        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_MISSION_ID = "mission_id";

        public static final String CREATE_CURRENT_MISSIONS_TABLE = "CREATE TABLE "
                + CurrentMissionEntry.TABLE_NAME + "("
                + CurrentMissionEntry._ID + " INTEGER PRIMARY KEY, "
                + CurrentMissionEntry.COLUMN_MISSION_ID + " INTEGER NOT NULL, "
                + "FOREIGN KEY(" + CurrentMissionEntry.COLUMN_MISSION_ID + ")"
                + " REFERENCES " + MissionsEntry.TABLE_NAME + "(" + MissionsEntry._ID + "));";

        //public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_URI, PATH_LEVELS);
    }
}