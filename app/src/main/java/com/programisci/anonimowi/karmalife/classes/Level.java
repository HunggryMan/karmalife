package com.programisci.anonimowi.karmalife.classes;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.widget.Toast;

import com.programisci.anonimowi.karmalife.data.KarmaLifeDbHelper;

import java.util.ArrayList;

import static com.programisci.anonimowi.karmalife.data.KarmaLifeContract.*;

/**
 * A class that describes level from db.
 *
 * Created by RaV on 25.11.2017.
 */

public class Level {

    private String name;
    private int value;
    private int expMin;
    private int expMax;

    public Level(String name, int value, int expMin, int expMax) {
        this.name = name;
        this.value = value;
        this.expMin = expMin;
        this.expMax = expMax;
    }

    public int getExpMax() {
        return expMax;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public int getExpMin() {
        return expMin;
    }

    private static boolean isTableEmpty(Context context) {
        SQLiteDatabase db = (new KarmaLifeDbHelper(context)).getReadableDatabase();

        Cursor cursor = db.query(
                LevelEntry.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null
        );

        return cursor.getCount() == 0;
    }

    public static ArrayList<Level> initialiseLevels() {


        ArrayList<Level> levels = new ArrayList<>();

        levels.add(new Level("Godfather of Evil", -6, Integer.MIN_VALUE, -5000));
        levels.add(new Level("Mandkind's nightmare", -5, -5000, -2500));
        levels.add(new Level("Why so nasty?", -4, -2500, -1700));
        levels.add(new Level("The Evil One", -3, -1700, -1000));
        levels.add(new Level("\"No\" Man", -2, -1000, -500));
        levels.add(new Level("Ordinary One", -1, -500, -100));
        levels.add(new Level("Neutral One", 0, -100, 0));
        levels.add(new Level("Ordinary One", 1, 0, 100));
        levels.add(new Level("NotSoOrdinary One", 2, 100, 250));
        levels.add(new Level("Nice Man", 3, 250, 400));
        levels.add(new Level("The Favourite One", 4, 400, 800));
        levels.add(new Level("Helpful hand", 5, 800, 1500));
        levels.add(new Level("Good soul", 6, 1500, 2300));
        levels.add(new Level("Godsend", 7, 2300, 3200));
        levels.add(new Level("Friend of Mankind", 8, 3200, 4100));
        levels.add(new Level("Ghandi", 9, 4100, Integer.MAX_VALUE));

        return levels;
    }
}
