package com.programisci.anonimowi.karmalife;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

public class AddMissionActivity extends AppCompatActivity {


    private EditText editTextMissionAdd;
    private ImageButton submitButtonMission;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_mission);

        initialize();

    }

    private void initialize(){
        editTextMissionAdd = findViewById(R.id.edit_text_mission_add);
        submitButtonMission = findViewById(R.id.submit_button_mission);
        submitButtonMission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String missionDesc = editTextMissionAdd.getText().toString();
                startActivity(new Intent(AddMissionActivity.this, MissionsActivity.class));
            }
        });


    }
}
